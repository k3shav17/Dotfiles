## Terminal

## Shortcuts

- ctrl + r = search for a specific term in the history
- history = commands history
- ctrl + a = start of the command
- ctrl + e = end of the command
- ctrl + l = gets the command prompt to the top of the window
- ctrl + z = to minimize the task which is currently running
- fg = brings the background task to foreground
- sudo !! -> to run the previous command with sudo permission

<hr>

## To generated random/sample files in a sequence

```bash
--- seq 5 (till the number you want to generate the files) | xargs -I {} touch file_name{}.extension
```

> Example: Lets say I need to create 5 img file with extension jpg, png or whatever then

```bash
seq 5 | xargs -I {} touch img_{}.png

```

<hr>

## To rename files with extensions

--- for x in *(extension/filenames); do mv -- "$x" "${x%.fileExt}.toNewExt"; done
> Example: Lets say I have text files with names file1 file2 file3 and I want to change them to mardown files then

```bash
for f in *(filenames/extension); do mv -- "$f" "{f%.oldext}.toNewExt"; done

```

<hr>

## Regular expressions

-- if you want get lines from text which is started with specific word, this syntax is used ^word.
-- same thing but want with end of the line then, this syntax word$.
-- get words which contains the specific letter then, letter l.
-- want to get words with specific combinations then, w[or]d. Words starting with w and ending with d and contains o or r in those will gets displayed.

-- If wants to series of words then, w[a-d]e. Words starting with w and ending with e contains any letter between a-d will gets displayed.

-- but if you want to display with capital letters then, w[A-D]e

<hr>

## vim bindings

a is to insert text after the cursor, A is to insert at the end of the line.
o is to insert on next line, O is to insert on previous line
u is to undo, ctrl-r is to redo
x is to delete a character, dd is to delete entire line
y is to copy the selected text, yy is to copy current line
p is to paste

:wq is to save and quit
:q is to quit
:q! is quit without saving
:w save the file
ZZ is same as :wq

// more to come

<hr>

## File permissions

rwx rwx rwx - There are three different types of permissions
r - read, w - write, x - execute

- And the persons who can access are first 3 refers to **user**, second 3 refers to **group** that they are in, third 3 refers to **everyone**.

- And each three adds up to 7, like r-4, w-3, x-1 == 7
- So, say if only you can access a file, then the code will be chmod 700 filename
- Example

 ```bash
 if you wants to give access to read permission to the group then the code will be 
 chmod 744
 ```

## Grep Commands (Global RegEx Print)

- The grep command is used to get the text or a string a in file
- We can pipe the grep or use it as normal command
- grep can be used with -c (gives count how many times that string appeared)
- -n (gives line numbers for the lines with the mentioned string)
- -v (to exclude the mentioned string from search)
- -i (ignore case to search the string)
- * to check the mentioned string in all the files in a directory
- -r to recursively search through files through directories

