vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.opt.backspace = "2"
vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.autowrite = true
vim.opt.cursorline = true
vim.opt.autoread = true

-- use spaces for tabs and whatnot
vim.opt.tabstop = 4
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.expandtab = true
vim.o.clipboard = "unnamedplus"

vim.cmd([[ set noswapfile ]])
vim.cmd([[ set termguicolors ]])
vim.cmd([[ set relativenumber ]])
vim.cmd([[ set shiftwidth=4 ]])
vim.cmd([[ set ignorecase ]])
vim.cmd([[ set incsearch ]])
vim.cmd([[ set smartcase ]])
vim.cmd([[ set showcmd ]])
vim.cmd([[ set showmode ]])
vim.cmd([[ set showmatch ]])
vim.cmd([[ set wildmenu ]])
vim.cmd([[ set spell ]])
vim.cmd([[ set number ]])
vim.cmd([[ set cmdheight=0 ]])
vim.cmd([[ set shell=/bin/zsh ]])
vim.cmd([[ set shellcmdflag=-c ]])
vim.cmd([[ set shellquote= ]])
vim.cmd([[ set shellxquote= ]])

--Line numbers
vim.wo.number = true

-- stop right-shift when errors/warning appear
vim.o.signcolumn = "yes"
vim.o.completeopt = "menuone,noselect,preview"

-- move selections
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv") -- Shift visual selected line down
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv") -- Shift visual selected line up

-- colorscheme picker
vim.keymap.set("n", "<C-n>", ":Telescope colorscheme<CR>")

-- remaps
vim.g.mapleader = " "

-- zig
-- vim.g.zig_fmt_autosave = 0

-- neo-tree setup
-- vim.keymap.set("n", "<leader>n", ":Neotree filesystem reveal right<CR>")

-- oil.nvim setup
vim.keymap.set("n", "<leader>N", ":Oil<CR>")
vim.keymap.set("n", "<leader>n", ':lua require("oil").toggle_float()<CR>')
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "<C-f>", "<C-f>zz")
vim.keymap.set("n", "<C-b>", "<C-b>zz")
vim.keymap.set("n", "Y", "yy")

-- lsp setup
vim.keymap.set("n", "K", vim.lsp.buf.hover)
vim.keymap.set("n", "gd", vim.lsp.buf.definition)
vim.keymap.set("n", "gD", vim.lsp.buf.declaration)
vim.keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, {})

-- see error
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float)

-- go to errors
vim.keymap.set("n", "[e", vim.diagnostic.goto_next)
vim.keymap.set("n", "]e", vim.diagnostic.goto_next)

-- disable default errors
vim.diagnostic.config({
	virtual_text = false,
})
