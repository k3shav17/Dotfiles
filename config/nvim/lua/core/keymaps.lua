-- Navigate vim panes better
vim.keymap.set('n', '<c-k>', ':wincmd k<CR>')
vim.keymap.set('n', '<c-j>', ':wincmd j<CR>')
vim.keymap.set('n', '<c-h>', ':wincmd h<CR>')
vim.keymap.set('n', '<c-l>', ':wincmd l<CR>')

vim.keymap.set('n', '<leader>h', ':nohlsearch<CR>')


vim.keymap.set('n', 'gd', ':goto_preview_definition<CR>')
vim.keymap.set('n', 'gt', ':goto_preview_type_definition<CR>')
vim.keymap.set('n', 'gi', ':goto_preview_implementation<CR>')
vim.keymap.set('n', 'gD', ':goto_preview_declaration<CR>')
vim.keymap.set('n', 'gr', ':goto_preview_references<CR>')

vim.api.nvim_set_keymap("n", "<leader>/", ":Commentary<CR>", {})
