require("whichkey").setup({
	keys = {
		{
			"<leader>w",
			function()
				require("which-key").show({ global = false })
			end,
			desc = "Buffer Local Keymaps (which-key)",
		},
	},
})
