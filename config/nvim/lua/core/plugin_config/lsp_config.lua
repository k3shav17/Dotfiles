require("mason-lspconfig").setup({
	ensure_installed = { "jdtls", "gopls", "lua_ls", "bashls", "ts_ls" },
})

local lspconfig = require("lspconfig")
local util = require("lspconfig/util")
local jdtls_path = "/home/keshavak/Downloads/"
local lsp_defaults = lspconfig.util.default_config

lsp_defaults.capabilities =
	vim.tbl_deep_extend("force", lsp_defaults.capabilities, require("cmp_nvim_lsp").default_capabilities())

require("lspconfig").lua_ls.setup({
	settings = {
		Lua = {
			diagnostics = {
				globals = { "vim" },
			},
			workspace = {
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.stdpath("config") .. "/lua"] = true,
				},
			},
		},
	},
})

require("lspconfig").ts_ls.setup({})

require("lspconfig").gopls.setup({
	on_attach = function(client)
		-- Enable formatting on save
		if client.server_capabilities.documentFormattingProvider then
			vim.cmd([[autocmd BufWritePre <buffer> lua vim.lsp.buf.format()]])
		end
	end,
})

require("lspconfig").jdtls.setup({

	jdtls = function(_, opts)
		vim.api.nvim_create_autocmd("FileType", {
			pattern = "java",
			callback = function()
				require("lazyvim.util").on_attach(function(_, buffer)
					vim.keymap.set(
						"n",
						"<leader>di",
						"<Cmd>lua require'jdtls'.organize_imports()<CR>",
						{ buffer = buffer, desc = "Organize Imports" }
					)
					vim.keymap.set(
						"n",
						"<leader>dt",
						"<Cmd>lua require'jdtls'.test_class()<CR>",
						{ buffer = buffer, desc = "Test Class" }
					)
					vim.keymap.set(
						"n",
						"<leader>dn",
						"<Cmd>lua require'jdtls'.test_nearest_method()<CR>",
						{ buffer = buffer, desc = "Test Nearest Method" }
					)
					vim.keymap.set(
						"v",
						"<leader>de",
						"<Esc><Cmd>lua require('jdtls').extract_variable(true)<CR>",
						{ buffer = buffer, desc = "Extract Variable" }
					)
					vim.keymap.set(
						"n",
						"<leader>de",
						"<Cmd>lua require('jdtls').extract_variable()<CR>",
						{ buffer = buffer, desc = "Extract Variable" }
					)
					vim.keymap.set(
						"v",
						"<leader>dm",
						"<Esc><Cmd>lua require('jdtls').extract_method(true)<CR>",
						{ buffer = buffer, desc = "Extract Method" }
					)
					vim.keymap.set(
						"n",
						"<leader>cf",
						"<cmd>lua vim.lsp.buf.formatting()<CR>",
						{ buffer = buffer, desc = "Format" }
					)
				end)

				local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":p:h:t")
				-- vim.lsp.set_log_level('DEBUG')
				local workspace_dir = "/home/keshavak/.workspace/" .. project_name -- See `:help vim.lsp.start_client` for an overview of the supported `config` options.
				local config = {
					-- The command that starts the language server
					-- See: https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line
					cmd = {

						"java", -- or '/path/to/java17_or_newer/bin/java'
						-- depends on if `java` is in your $PATH env variable and if it points to the right version.

						"-javaagent:/home/keshavak/.local/share/java/lombok.jar",
						-- '-Xbootclasspath/a:/home/jake/.local/share/java/lombok.jar',
						"-Declipse.application=org.eclipse.jdt.ls.core.id1",
						"-Dosgi.bundles.defaultStartLevel=4",
						"-Declipse.product=org.eclipse.jdt.ls.core.product",
						"-Dlog.protocol=true",
						"-Dlog.level=ALL",
						-- '-noverify',
						"-Xms1g",
						"--add-modules=ALL-SYSTEM",
						"--add-opens",
						"java.base/java.util=ALL-UNNAMED",
						"--add-opens",
						"java.base/java.lang=ALL-UNNAMED",
						"-jar",
						vim.fn.glob("/home/keshavak/Downloads/plugins/org.eclipse.equinox.launcher_*.jar"),
						-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                       ^^^^^^^^^^^^^^
						-- Must point to the                                                     Change this to
						-- eclipse.jdt.ls installation                                           the actual version

						"-configuration",
						"/home/keshavak/Downloads/config_linux",
						-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        ^^^^^^
						-- Must point to the                      Change to one of `linux`, `win` or `mac`
						-- eclipse.jdt.ls installation            Depending on your system.

						-- See `data directory configuration` section in the README
						"-data",
						workspace_dir,
					},

					-- This is the default if not provided, you can remove it. Or adjust as needed.
					-- One dedicated LSP server & client will be started per unique root_dir
					root_dir = require("jdtls.setup").find_root({ ".git", "mvnw", "gradlew" }),

					-- Here you can configure eclipse.jdt.ls specific settings
					-- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
					-- for a list of options
					settings = {
						java = {},
					},
					handlers = {
						["language/status"] = function(_, result)
							-- print(result)
						end,
						["$/progress"] = function(_, result, ctx)
							-- disable progress updates.
						end,
					},
				}
				require("jdtls").start_or_attach(config)
			end,
		})
		return true
	end,

	on_attach = function(client)
		-- Enable formatting on save
		if client.server_capabilities.documentFormattingProvider then
			vim.cmd([[autocmd BufWritePre <buffer> lua vim.lsp.buf.format()]])
		end
	end,
})

require("lspconfig").bashls.setup({})

vim.api.nvim_create_autocmd("LspAttach", {
	group = vim.api.nvim_create_augroup("UserLspConfig", {}),
	callback = function(ev)
		-- Enable completion triggered by <c-x><c-o>
		vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

		-- Buffer local mappings.
		-- See `:help vim.lsp.*` for documentation on any of the below functions
		local opts = { buffer = ev.buf }
		vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
		vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
		vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
		vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
		vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, opts)
		vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, opts)
		vim.keymap.set("n", "<space>wl", function()
			print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		end, opts)
		vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, opts)
		vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)
		vim.keymap.set({ "n", "v" }, "<space>ca", vim.lsp.buf.code_action, opts)
		vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
		vim.keymap.set("n", "<space>f", function()
			vim.lsp.buf.format({ async = true })
		end, opts)
	end,
})
