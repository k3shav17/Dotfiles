# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
*i*) ;;
*) return ;;
esac

MY_BASH_BLUE="\033[0;33m"
MY_BASH_NOCOLOR="\033[0m"

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

#History format
HISTTIMEFORMAT=$(echo -e ${MY_BASH_BLUE} %y-%m-%d %T $MY_BASH_NOCOLOR)

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
### SHOPT
shopt -s autocd  # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s expand_aliases # expand aliases
shopt -s checkwinsize   # checks term size when bash regains control
shopt -s histappend     # append to the history file, don't overwrite it

# ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
  debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
xterm-color | *-256color) color_prompt=yes ;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
  if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
  else
    color_prompt=
  fi
fi

if [ "$color_prompt" = yes ]; then
  PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm* | rxvt*)
  PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
  ;;
*) ;;

esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  # alias ls='ls --color=auto'
  alias ls='lsd'
  #alias dir='dir --color=auto'
  #alias vdir='vdir --color=auto'

  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#### My Changes ####

### TMUX-SESSION ###
# tmux

## Cargo package prompt
#. "$HOME/.cargo/env"

### RANDOM COLOR SCRIPT ###
#colorscript random

eval "$(starship init bash)"

### ALIASES ###
alias aptup='sudo apt update;sudo apt upgrade'
alias ins='sudo apt install'
alias upd='sudo apt update'
alias upg='sudo apt upgrade'
alias remove='sudo apt remove'
alias autoremove='sudo apt autoclean;sudo apt autoremove;sudo apt clean'
alias fullup='sudo apt full-upgrade'
alias distup='sudo apt dist-upgrade'
alias ncdu='ncdu --color dark'

### Nala aliases
# alias aptup='sudo nala update;sudo nala upgrade'
# alias ins='sudo nala install'
# alias upd='sudo nala update'
# alias upg='sudo nala upgrade'
# alias remove='sudo nala remove'
# alias search='sudo nala search'
# alias clean='sudo nala clean'
# alias show='nala show'

## other aliases
alias cl='clear' # clears the console and generates a random colorscript
alias cdc='cd'                   # clears the console and goes to home directory
alias lla='lsd -lAh'                   # shows all the files and directories in list format
alias llt='lsd -lt'                    # same as lt but shows in list format
alias llx='lsd -lX'                    # same as ls but shows in list format
alias lr='lsd -lRA'                    # shows the files in the child directories recursively
alias lx='lsd -X'                      # shows the files based on file extensions
alias lt='lsd -t'                      # shows the files or directories based on time it is modified
# alias lss='lsd | sort'
# alias lsa='lsd -lAh | sort'
alias ch='clear;history -c' # clears the console and removes all the commands history
alias suspend='sudo pm-suspend'

### alias for snap packages
alias sins='sudo snap install'
alias srem='sudo snap remove'
alias sref='sudo snap refresh --color=always'

## alias for history command
alias his='history'
alias hisl='history | less' # scrollable list

## alias for network commands
alias ipc='ip -c address'                            # shows the ip address with IP address in color to find easily
alias dnstat='sudo systemd-resolve --statistics'     # shows the dns statistics
alias flushdns='sudo systemd-resolve --flush-caches' # flushes the dns cache

## alias for mongodb commands
alias status='sudo systemctl status mongod'
alias startdb='sudo systemctl start mongod'
alias reloaddb='sudo systemctl daemon-reload'
alias enabledb='sudo systemctl enable mongod'
alias restartdb='sudo systemctl restart mongod'
alias stopdb='sudo systemctl stop mongod'

## alias for kitty terminals kittens
alias icat='kitty +kitten icat'

# # ex - archive extractor
# # usage: ex <file>
ex() {
  if [ -f $1 ]; then
    case $1 in
    *.tar.bz2) tar xjf $1 ;;
    *.tar.gz) tar xzf $1 ;;
    *.bz2) bunzip2 $1 ;;
    *.rar) unrar x $1 ;;
    *.gz) gunzip $1 ;;
    *.tar) tar xf $1 ;;
    *.tbz2) tar xjf $1 ;;
    *.tgz) tar xzf $1 ;;
    *.zip) unzip $1 ;;
    *.Z) uncompress $1 ;;
    *.7z) 7z x $1 ;;
    *) echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#export PAGER='most'

export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion


## SSH to termux-android
alias termux='ssh u0_a1022@192.168.1.34 -p 8022'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash



















