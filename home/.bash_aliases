#!/usr/bin/bash
#  _  ___________  _   _ _ _____
# | |/ /___ / ___|| | | / |___  |    Gitlab: https://gitlab.com/k3shav17
# | ' /  |_ \___ \| |_| | |  / /     Github: https://github.com/k3shav17
# | . \ ___) |__) |  _  | | / /
# |_|\_\____/____/|_| |_|_|/_/

### ALIASES ###

## pacman aliases
# alias pacup='sudo pacman -Syyu'
# alias pacin='sudo pacman -Sy'
# alias repac='sudo pacman -R'
# alias repacd='sudo pacman -Rcns'
# alias list='pacman -Q'

## dnf aliases
alias dnfin='sudo dnf install'
alias dnfrem='sudo dnf remove'
alias dnfup='sudo dnf update && sudo dnf upgrade'
alias dnfcl='sudo dnf autoremove'

# alias aptup='sudo apt update;sudo apt upgrade'
# alias ins='sudo apt install'
# alias upd='sudo apt update'
# alias upg='sudo apt upgrade'
# alias remove='sudo apt remove'
# alias autoremove='sudo apt autoclean;sudo apt autoremove;sudo apt clean'
# alias fullup='sudo apt full-upgrade'
# alias distup='sudo apt dist-upgrade'

### Nala aliases
# alias aptup='sudo nala update; sudo nala upgrade'
# alias ins='sudo nala install'
# alias upd='sudo nala update'
# alias upg='sudo nala upgrade'
# alias remove='sudo nala remove'
# alias search='sudo nala search'
# alias clean='sudo nala clean'
# alias show='nala show'

## other aliases
# alias cl='clear;colorscript -e random'                  # clears the console and generates a random colorscript
alias cl='clear'
# alias cl="printf '\E[H\E[3J'; colorscript -e random"     # this command is to clear the scrollback buffer in kitty
alias cdc='cd && cl'                                     # clears the console and goes to home directory
alias ch='clear;history -c'                              # clears the console and removes all the commands history


## alias List storage
alias lla='lsd -lAh --group-directories-first'                   # shows all the files and directories in list format
alias ll='lsd -l --group-directories-first'                      # shows the directories in list format
alias llt='lsd -lt --group-directories-first'                    # same as lt but shows in list format
alias llx='lsd -lX'                    # same as lx but shows in list format
alias lr='lsd -lRA'                    # shows the files in the child directories recursively
alias lx='lsd -X'                      # shows the files based on file extensions
alias lt='lsd -t'                      # shows the files or directories based on time it is modified
alias l.='lsd -a | egrep "^\."'        # shows the list of dotfiles and directories
alias la='lsd -A --group-directories-first'

### alias for snap packages
#alias sins='sudo snap install'
#alias srem='sudo snap remove'
#alias sref='sudo snap refresh --color=always'

## alias for history command
alias his='history'
alias hisl='history | less' # scrollable list
alias hic='history -c'

## alias for network commands
alias ipc='ip -c address'                            # shows the ip address with IP address in color to find easily
alias dnstat='sudo systemd-resolve --statistics'     # shows the dns statistics
alias flushdns='sudo systemd-resolve --flush-caches' # flushes the dns cache

## alias for mongodb commands
alias statusdb='sudo systemctl status mongod'
alias startdb='sudo systemctl start mongod'
alias reloaddb='sudo systemctl daemon-reload'
alias enabledb='sudo systemctl enable mongod'
alias restartdb='sudo systemctl restart mongod'
alias stopdb='sudo systemctl stop mongod'

## MySQL
alias condb='sudo mysql -u kesh -p'

## alias for kitty terminals kittens
alias icat='kitty +kitten icat'

## navigation
alias ..='cd ..'
alias .2='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

## miscellaneous
alias vim='nvim'
alias svim='sudo nvim'
alias suspend='sudo pm-suspend'
alias df='df -h'                             # Shows the disk space in human readable form
alias du='du -sh'                            # shows the size of specific file or folder
alias vb='nvim ~/.bashrc'
alias vba='nvim ~/.bash_aliases'
alias exip='curl icanhazip.com'
alias mkd='mkdir -pv'
alias ncdu='ncdu --color dark'
alias cat='bat'
alias x='exit'

## Shutdown or power off
alias ssn='sudo shutdown now'
alias sr='sudo reboot'

## bash restart
alias sbash='source ~/.bashrc'

## get error messages from journalctl
alias jctl='journalctl -p 3 -xb'

## Git aliases
alias glog='git log --graph --decorate --oneline'
alias clone='git clone'
alias add='git add'
alias commit='git commit -m'
alias pull='git pull origin'
alias push='git push'
alias checkout='git checkout'
alias stat='git status'
alias switch='git switch'
alias fetch='git fetch'
alias branch='git branch'

## config aliases
alias vial='nvim ~/.config/alacritty/alacritty.yml'
alias viki='nvim ~/.config/kitty/kitty.conf'
alias vico='nvim ~/.conkyrc'
alias coal='code ~/.config/alacritty/alacritty.yml'
alias coki='code ~/.config/kitty/kitty.conf'
alias vinit='nvim ~/.config/nvim/init.vim'

## node-npm packages alias
alias dict='npx cli-dic'

## Aliases for proton vpn
alias vpnlogin='protonvpn-cli login'
alias vpncon='protonvpn-cli connect'
alias vpnstat='protonvpn-cli status'
alias vpndisc='protonvpn-cli disconnect'
alias vpnkill0='protonvpn-cli ks --on'
alias vpnkill1='protonvpn-cli ks --permanent'
alias vpnkill2='protonvpn-cli ks --off'

## Optimus manager
alias opnvidia='optimus-manager --switch nvidia'
alias ophybrid='optimus-manager --switch hybrid'
alias opintegrate='optimus-manager --switch integrated'
alias opmode='optimus-manager --print-mode'

## EnvyControl
#alias envyintegrate='sudo envycontrol -s integrated'
#alias envyhybrid='sudo envycontrol -s hybrid'
#alias envynvidia='sudo envycontrol -s nvidia'
## SSH
alias ssh-moto='ssh u0_a196@192.168.1.37 -p8022'
