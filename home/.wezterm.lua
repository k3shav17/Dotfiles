-- Pull in the wezterm module
local wezterm = require("wezterm")

-- config holder
local config = wezterm.config_builder()


config.font = wezterm.font("Delugia")
config.font_size = 13
config.harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' }
-- Use this for a zero with a line through it rather than a dot
-- when using the Fira Code font
config.harfbuzz_features = { 'zero' }

config.enable_tab_bar = false
config.window_decorations = "RESIZE"

config.window_background_opacity = 0.8

config.cursor_blink_ease_in = "Linear"
config.cursor_blink_ease_out = "Linear"
config.cursor_blink_rate = 750
config.cursor_thickness = 1
config.default_cursor_style = 'BlinkingUnderline'

-- config.color_scheme = "nordfox"
-- config.color_scheme = 'Gruvbox Dark (Gogh)'
-- config.color_scheme = 'Catppuccin Frappé (Gogh)'
-- config.color_scheme = 'Catppuccin Macchiato'
-- config.color_scheme = 'Catppuccin Macchiato (Gogh)'
config.color_scheme = 'Catppuccin Mocha (Gogh)'

-- config.colors = {
--   foreground = "#CBE0F0",
--   background = "#011423",
--   cursor_bg = "#47FF9C",
--   cursor_border = "#47FF9C",
--   cursor_fg = "#011423",
--   selection_bg = "#033259",
--   selection_fg = "#CBE0F0",
--   ansi = { "#214969", "#E52E2E", "#44FFB1", "#FFE073", "#0FC5ED", "#a277ff", "#24EAF7", "#24EAF7" },
--   brights = { "#214969", "#E52E2E", "#44FFB1", "#FFE073", "#A277FF", "#a277ff", "#24EAF7", "#24EAF7" },
-- }

return config
