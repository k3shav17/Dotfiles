## Dotfiles

- Repository related to Linux config files.
    1. Bash_history - History of the bash commands
    2. Bashrc - is a bash config file, in which all the configuration of the bash shell lives and starts up when the shell is started.
    3. Tips - Is a folder related to some linux terminal commands, as of now only few are there and will be adding more soon.
- [Kitty](https://sw.kovidgoyal.net/kitty/) Conf
  - Configuration for the kitty terminal emulator.

- [Starship](starship.rs)
  - starship toml file is a terminal prompt scritp, this toml file is a configuration of the starship prompt.

- Neovim
    - Text editor for productivity, configured the neovim for java and some other languages
- Config files
    - Dash to panel config file
    - lsd config files
    - color schemes by DT
    - bashrc for termux on android
